import React from 'react';
import ReactDOM from 'react-dom';
import App from './App/App';

fetch('http://localhost:8080/login', {
    method: 'post',
    body: {login: 'ok', password: 'ok'}
  })
  .then(function (data) {
    console.log('Request succeeded with JSON response', data);
  })
  .catch(function (error) {
    console.log('Request failed', error);
  });


ReactDOM.render(
    <App />,
  document.getElementById('root')
);
