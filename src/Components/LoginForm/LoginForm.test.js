mockserver.start_mockserver({
                serverPort: 1080,
                proxyPort: 1090,
                verbose: true
            });

console.log('test')

mockServerClient("localhost", 8080).mockAnyResponse({
    "httpRequest": {
        "method": "POST",
        "path": "/login",
        "body": {
            "type": "JSON",
            "value": JSON.stringify({ Username: "User", Password: "Password" }),
            "matchType": "STRICT"
        }
    },
    "httpResponse": {
        "statusCode": 200,
        "headers": [
            {
                "name": "Content-Type",
                "values": ["application/json; charset=utf-8"]
            },
            {
                "name": "Cache-Control",
                "values": ["public, max-age=86400"]
            }
        ],
        "body": JSON.stringify({ Auth: "Logged", Language: "EN" }),
        "delay": {
            "timeUnit": "SECONDS",
            "value": 1
        }
    }
});
