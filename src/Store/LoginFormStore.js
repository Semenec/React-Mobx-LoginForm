import { observable, action } from 'mobx';


class LoginFormStore {

	@observable loginStatus = null;
	@observable loginInput = '';
	@observable passwordInput = '';

	@action('loginChange')
		loginChange(e) {
			this.loginInput = e
		}
	@action('passwordChange')
		passwordChange(e) {
			this.passwordInput = e
		}
	@action('getLogin')
		getLogin() {
			console.log({username: this.loginInput, password: this.passwordInput})
		}
}

export default new LoginFormStore();
